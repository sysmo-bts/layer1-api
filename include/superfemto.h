#ifndef SUPERFEMTO_H__
#define SUPERFEMTO_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                  Includes                                *
 ****************************************************************************/
#include <stdint.h>

#include "gsml1const.h"

#define SUPERFEMTO_API(x,y,z)    ((x << 16) | (y << 8) | z)
#define SUPERFEMTO_API_VERSION   SUPERFEMTO_API(5,1,0)

/****************************************************************************
 *                                   Const                                  *
 ****************************************************************************/

/****************************************************************************
 * Enum: SuperFemto_Status_t
 ************************************************************************//** 
 * 
 * Status used by the SuperFemto.
 * 
 * @ingroup superfemto_api
 *
 ****************************************************************************/
typedef GsmL1_Status_t SuperFemto_Status_t;

 
/****************************************************************************
 * Enum : SuperFemto_PrimId_t
 ************************************************************************//**
 *   
 *   System management primitives.
 *
 *   @ingroup superfemto_api
 *   
 ****************************************************************************/
typedef enum SuperFemto_PrimId_t
{ 
    SuperFemto_PrimId_SystemInfoReq,      ///< REQ: System information    
    SuperFemto_PrimId_SystemInfoCnf,      ///< CNF: System information
    SuperFemto_PrimId_SystemFailureInd,   ///< IND: System failure indication    
    SuperFemto_PrimId_ActivateRfReq,      ///< REQ: Activation of the RF section
    SuperFemto_PrimId_ActivateRfCnf,      ///< CNF: Activation of the RF section
    SuperFemto_PrimId_DeactivateRfReq,    ///< REQ: Deactivation of the RF section
    SuperFemto_PrimId_DeactivateRfCnf,    ///< CNF: Deactivation of the RF section    
    SuperFemto_PrimId_SetTraceFlagsReq,   ///< REQ: Set the verbosity of the system    
    SuperFemto_PrimId_RfClockInfoReq,     ///< REQ: Information about the RF clock sub-system
    SuperFemto_PrimId_RfClockInfoCnf,     ///< CNF: Returns the RF clock sub-system information
    SuperFemto_PrimId_RfClockSetupReq,    ///< REQ: Configure the RF clock sub-system
    SuperFemto_PrimId_RfClockSetupCnf,    ///< CNF: Confirm the configuration of the RF clock sub-system
    SuperFemto_PrimId_Layer1ResetReq,     ///< REQ: Trigger a reset of the GSM layer 1 library
    SuperFemto_PrimId_Layer1ResetCnf,     ///< CNF: Confirm that the GSM layer 1 library was reset
    SuperFemto_PrimId_GetTxCalibTblReq,   ///< REQ: Get the TX gain calibration table 
    SuperFemto_PrimId_GetTxCalibTblCnf,   ///< CNF: Returns TX gain calibration table
    SuperFemto_PrimId_SetTxCalibTblReq,   ///< REQ: Set the TX gain calibration table 
    SuperFemto_PrimId_SetTxCalibTblCnf,   ///< CNF: Confirm the use of the new TX gain calibration table 
    SuperFemto_PrimId_GetRxCalibTblReq,   ///< REQ: Get the RX level calibration table 
    SuperFemto_PrimId_GetRxCalibTblCnf,   ///< CNF: Returns RX level calibration table
    SuperFemto_PrimId_SetRxCalibTblReq,   ///< REQ: Set the RX level calibration table 
    SuperFemto_PrimId_SetRxCalibTblCnf,   ///< CNF: Confirm the use of the new RX level calibration table 
    SuperFemto_PrimId_MuteRfReq,          ///< REQ: Mute/Unmute the RF section
    SuperFemto_PrimId_MuteRfCnf,          ///< CNF: Confirm the mutin/unmiting of the the RF section
    SuperFemto_PrimId_SetRxAttenReq,      ///< REQ: Set the RX attenuation
    SuperFemto_PrimId_SetRxAttenCnf,      ///< CNF: Confirm the configuration of the RX attenuation
    SuperFemto_PrimId_NUM

} SuperFemto_PrimId_t;

/****************************************************************************
 * Enum : SuperFemto_ClkSrcId_t
 ************************************************************************//**
 *   
 *   Clock source intifiers.
 *
 *   @ingroup superfemto_api
 *   
 ****************************************************************************/
typedef enum SuperFemto_ClkSrcId_t
{
    // RF Diagnostic Primitives 
    SuperFemto_ClkSrcId_None = 0,               ///< None
    SuperFemto_ClkSrcId_Ocxo,                   ///< Optional on-board OCXO
    SuperFemto_ClkSrcId_Tcxo,                   ///< Optional on-board TCXO
    SuperFemto_ClkSrcId_External,               ///< Multi-Trx external clock
    SuperFemto_ClkSrcId_GpsPps,                 ///< GPS PPS 
    SuperFemto_ClkSrcId_Trx,                    ///< TRX clock
    SuperFemto_ClkSrcId_Rx,                     ///< RX clock
    SuperFemto_ClkSrcId_Edge,                   ///< Debug edge connector clock
    SuperFemto_ClkSrcId_NetList                 ///< Network listening

} SuperFemto_ClkSrcId_t;

/****************************************************************************
 *                                  Types                                   *
 ****************************************************************************/

/****************************************************************************
 * Struct : SuperFemto_SystemInfoCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the information about the system.
 * 
 * @ingroup superfemto_api_prim_sys
 *
****************************************************************************/
typedef struct SuperFemto_SystemInfoCnf
{
    SuperFemto_Status_t status;       ///< Status
   
    struct
    {
        uint8_t major;              ///< DSP firmware major version number 
        uint8_t minor;              ///< DSP firmware minor version number
        uint8_t build;              ///< DSP firmware build version number
    } dspVersion;

    struct
    {
        uint8_t major;              ///< FPGA firmware major version number 
        uint8_t minor;              ///< FPGA firmware minor version number
        uint8_t build;              ///< FPGA firmware build version number
    } fpgaVersion;
    
    struct
    {
        uint8_t rev;                ///< Board reversion number 
        uint8_t option;             ///< Board option number
    } boardVersion;
    
} SuperFemto_SystemInfoCnf_t;
 
/****************************************************************************
 * Struct : SuperFemto_SystemFailureInd_t
 ************************************************************************//**
 * 
 * Structure is used to indicate a system failure.
 * 
 * @ingroup superfemto_api_prim_sys
 *
****************************************************************************/
typedef struct SuperFemto_SystemFailureInd
{
   SuperFemto_Status_t status;          ///< Status

} SuperFemto_SysemFailureInd_t;

/****************************************************************************
 * Struct : SuperFemto_ActivateRfReq_t
 ************************************************************************//**
 *
 * This primitive is used to request the activation the RF section of the system.
 *
 * @ingroup superfemto_api
 *
 ***************************************************************************/
typedef struct SuperFemto_ActivateRfReq
{
    // Maximum cell Size
	uint8_t u8MaxCellSize;				///< Maximum cell size in qbits (1 qbit = 138.4 meters, max 90 qbits)

	// Timing source (FN/TN)
    struct
    {
        uint8_t u8TimSrc;               ///< Timing source (0:Slave/Get timing from remote master BTS, 1:Master/Generates its own timing)
    } timing;
    
    // Message queue options
    struct {
        uint8_t u8UseTchMsgq;           ///< Set to '1' to use a separate MSGQUEUE for TCH primitives
        uint8_t u8UsePdtchMsgq;         ///< Set to '1' to use a separate MSGQUEUE for PDTCH primitives
    } msgq;
    
    // RF options
    struct
    {
        int iClkCor;                    ///< Clock correction value in PPB.
        SuperFemto_ClkSrcId_t clkSrc;   ///< Clock source (0:None, 1:OCXO, 2:TCXO, 3:External, 4:GPS PPS, 5:reserved, 6:RX, 7:Edge)
    
        float fMaxTxPower;              ///< Nominal maximum TX power in dBm
        uint8_t u8UseExtAtten;          ///< Use the external attenuator to control TX power (0:No, 1:Yes)
    } rfTrx;

} SuperFemto_ActivateRfReq_t;

/****************************************************************************
 * Struct : SuperFemto_ActivateRfCnf_t
 ************************************************************************//**
 *
 * This primitive is used to confirm the activation the RF section of the system.
 *
 * @ingroup superfemto_api
 *
 ***************************************************************************/
typedef struct SuperFemto_ActivateRfCnf
{
     SuperFemto_Status_t status;      ///< Status
    
} SuperFemto_ActivateRfCnf_t;

/****************************************************************************
 * Struct : SuperFemto_DeactivateRfCnf_t
 ************************************************************************//**
 *
 * This primitive is used to confirm the deactivation the RF section of the system.
 *
 * @ingroup superfemto_api
 *
 ***************************************************************************/
typedef struct SuperFemto_DeactivateRfCnf
{
     SuperFemto_Status_t status;      ///< Status
    
} SuperFemto_DeactivateRfCnf_t;

/****************************************************************************
 * Struct : SuperFemto_SetTraceFlagsReq_t
 ************************************************************************//**
 *
 * This primitive is used to setup the trace flag values. 
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_SetTraceFlagsReq
{
    uint32_t u32Tf;                 ///< Trace flag level 
} SuperFemto_SetTraceFlagsReq_t;

/****************************************************************************
 * Struct : SuperFemto_RfClockInfoReq
 ************************************************************************//**
 *
 * This primitive is used to retrive information about the RF clock
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_RfClockInfoReq
{        
    uint8_t u8RstClkCal;                ///< Set to '1' to reset the calibration module

} SuperFemto_RfClockInfoReq_t;

/****************************************************************************
 * Struct : SuperFemto_RfClockSetupReq
 ************************************************************************//**
 *
 * This primitive is used to configure the RF clock sub-system.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_RfClockSetupReq
{       
    // RF clock options
    struct
    {
        int iClkCor;                    ///< Clock correction value in PPB.
        SuperFemto_ClkSrcId_t clkSrc;   ///< Clock source (0:None, 1:OCXO, 2:TCXO, 3:External, 4:GPS PPS, 5:reserved, 6:RX, 7:Edge)
    } rfTrx;
    
    // RF clock calibration
    struct {
        SuperFemto_ClkSrcId_t clkSrc;   ///< Reference clock source (0:Off, 1:OCXO, 2:TCXO, 3:External, 4:GPS PPS, 5:reserved, 6:reserved, 7:Edge, 8:NL)
    } rfTrxClkCal;    

} SuperFemto_RfClockSetupReq_t;

/****************************************************************************
 * Struct : SuperFemto_RfClockSetupCnf
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the RF clock sub-system.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_RfClockSetupCnf
{        
     SuperFemto_Status_t status;   ///< Status of the RF-CLOCK-SETUP-REQ

} SuperFemto_RfClockSetupCnf_t;


/****************************************************************************
 * Struct : SuperFemto_RfClockInfoCnf
 ************************************************************************//**
 *
 * This primitive is used to send back information on the RF clock sub-system.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_RfClockInfoCnf
{
   // RF clock options
    struct
    {
        int iClkCor;                    ///< Clock correction value in PPB.
        SuperFemto_ClkSrcId_t clkSrc;   ///< Clock source (0:None, 1:OCXO, 2:TCXO, 3:External, 4:GPS PPS, 5:reserved, 6:RX, 7:Edge)
    } rfTrx;

    // RF clock calibration
    struct {
        SuperFemto_ClkSrcId_t clkSrc;   ///< Reference clock source (0:Off, 1:OCXO, 2:TCXO, 3:External, 4:GPS PPS, 5:reserved, 6:reserved, 7:Edge, 8:NL)
    
        int     iClkErr;                ///< RF clock error (PPB).
        int     iClkErrRes;             ///< Clock error resolution (PPT).
    } rfTrxClkCal;      

} SuperFemto_RfClockInfoCnf_t;

/****************************************************************************
 * Struct : SuperFemto_Layer1ResetReq_t
 ************************************************************************//**
 *
 * Structure is used to request a reset of the layer 1 module.
 *
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_Layer1ResetReq
{
    uint32_t u32Reserved;

} SuperFemto_Layer1ResetReq_t;

/****************************************************************************
 * Struct : SuperFemto_Layer1ResetCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the reset of the layer 1 module.
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_Layer1ResetCnf
{
    GsmL1_Status_t status;

} SuperFemto_Layer1ResetCnf_t;



/****************************************************************************
 * Struct : SuperFemto_GetTxCalibTblReq_t
 ************************************************************************//**
 * 
 * Structure is used to get the TX gain calibration table.
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_GetTxCalibTblReq
{
    GsmL1_FreqBand_t freqBand;  ///< GSM Frequency band

} SuperFemto_GetTxCalibTblReq_t;

/****************************************************************************
 * Struct : SuperFemto_GetTxCalibTblCnf_t
 ************************************************************************//**
 * 
 * Structure is used to returns the TX gain calibration table.
 *
 *  TX_Gain = m[band] x (fTxPowerLevel + fGainCorrVsArfcn[band][arfcn]) + b[band]
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_GetTxCalibTblCnf
{
    GsmL1_Status_t status;
    GsmL1_FreqBand_t freqBand;      ///< GSM Frequency band

    float fTxGainGmsk[80];          ///< Gain setting for GMSK output level from +50dBm to -29 dBm
    float fTx8PskCorr;              ///< Gain adjustment for 8 PSK (default to +3.25 dB)
    float fTxExtAttCorr[31];        ///< Gain adjustment for external attenuator (0:@1dB, 1:@2dB, ..., 31:@32dB)
    float fTxRollOffCorr[374];      /**< Gain correction for each ARFCN
                                                for GSM-850 : 0=128, 1:129, ..., 123:251, [124-373]:unused
                                                for GSM-900 : 0=955, 1:956, ...,  70:1, ..., 317:956, [318-373]:unused
                                                for DCS-1800: 0=512, 1:513, ..., 373:885
                                                for PCS-1900: 0=512, 1:513, ..., 298:810, [299-373]:unused */

	uint8_t u8DspMajVer;           	///< DSP firmware major version (0 if unkown)
	uint8_t u8DspMinVer;           	///< DSP firmware minor version (0 if unkown)
	uint8_t u8FpgaMajVer;          	///< FPGA firmware major version (0 if unkown)
	uint8_t u8FpgaMinVer;          	///< FPGA firmware minor version (0 if unkown)
} SuperFemto_GetTxCalibTblCnf_t;

/****************************************************************************
 * Struct : SuperFemto_SetTxCalibTblReq_t
 ************************************************************************//**
 * 
 * Structure is used to set the TX gain calibration table.
 *
 *  TX_Gain = m[band] x (fTxPowerLevel + fGainCorrVsArfcn[band][arfcn]) + b[band]
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_SetTxCalibTblReq
{
    GsmL1_FreqBand_t freqBand;      ///< GSM Frequency band

    float fTxGainGmsk[80];          ///< Gain setting for GMSK output level from +50dBm to -29 dBm
    float fTx8PskCorr;              ///< Gain adjustment for 8 PSK (default to +3.25 dB)
    float fTxExtAttCorr[31];        ///< Gain adjustment for external attenuator (0:@1dB, 1:@2dB, ..., 31:@32dB)
    float fTxRollOffCorr[374];      /**< Gain correction for each ARFCN
                                                for GSM-850 : 0=128, 1:129, ..., 123:251, [124-373]:unused
                                                for GSM-900 : 0=955, 1:956, ...,  70:1, ..., 317:956, [318-373]:unused
                                                for DCS-1800: 0=512, 1:513, ..., 373:885
                                                for PCS-1900: 0=512, 1:513, ..., 298:810, [299-373]:unused */

    uint8_t u8DspMajVer;           	///< DSP firmware major version (0 if unkown)
    uint8_t u8DspMinVer;           	///< DSP firmware minor version (0 if unkown)
    uint8_t u8FpgaMajVer;          	///< FPGA firmware major version (0 if unkown)
    uint8_t u8FpgaMinVer;          	///< FPGA firmware minor version (0 if unkown)
} SuperFemto_SetTxCalibTblReq_t;

/****************************************************************************
 * Struct : SuperFemto_SetTxCalibTblCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the use of the new TX gain calibration table.
 *
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_SetTxCalibTblCnf
{
    GsmL1_Status_t status;     
    GsmL1_FreqBand_t freqBand;      ///< GSM Frequency band                                     
} SuperFemto_SetTxCalibTblCnf_t;
       
/****************************************************************************
 * Struct : SuperFemto_GetRxCalibTblReq_t
 ************************************************************************//**
 * 
 * Structure is used to get the RX level calibration table.
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_GetRxCalibTblReq
{
    GsmL1_FreqBand_t freqBand;  ///< GSM Frequency band
    uint8_t bUplink;            ///< Direction (0:Downlink, 1:Uplink)

} SuperFemto_GetRxCalibTblReq_t;

/****************************************************************************
 * Struct : SuperFemto_GetRxCalibTblCnf_t
 ************************************************************************//**
 * 
 * Structure is used to returns the RX level calibration table.
 *
 *  RSSI = RxLevel + fRssiCorrVsArfcn[band][arfcn]
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_GetRxCalibTblCnf
{
    GsmL1_Status_t status;
    GsmL1_FreqBand_t freqBand;      ///< GSM Frequency band
    uint8_t bUplink;                ///< Direction (0:Downlink, 1:Uplink)  
    
    float fExtRxGain;               ///< External RX gain
    float fRxMixGainCorr;           ///< Mixer gain error compensation
    float fRxLnaGainCorr[3];        ///< LNA gain error compensation (1:@-12 dB, 2:@-24 dB, 3:@-36 dB)
    float fRxRollOffCorr[374];      /***< Frequency roll-off compensation
                                                for GSM-850 : 0=128, 1:129, ..., 123:251, [124-373]:unused
                                                for GSM-900 : 0=955, 1:956, ...,  70:1, ..., 317:956, [318-373]:unused
                                                for DCS-1800: 0=512, 1:513, ..., 373:885
                                                for PCS-1900: 0=512, 1:513, ..., 298:810, [299-373]:unused */                                                
    uint8_t u8IqImbalMode;          ///< IQ imbalance mode (0:off, 1:on, 2:auto)
    uint16_t u16IqImbalCorr[4];    	///< IQ imbalance compensation

    uint8_t u8DspMajVer;           	///< DSP firmware major version (0 if unkown)
    uint8_t u8DspMinVer;           	///< DSP firmware minor version (0 if unkown)
    uint8_t u8FpgaMajVer;          	///< FPGA firmware major version (0 if unkown)
    uint8_t u8FpgaMinVer;          	///< FPGA firmware minor version (0 if unkown)
} SuperFemto_GetRxCalibTblCnf_t;

/****************************************************************************
 * Struct : SuperFemto_SetRxCalibTblReq_t
 ************************************************************************//**
 * 
 * Structure is used to set the RX level calibration table.
 *
 *  RSSI = RxLevel + fRssiCorrVsArfcn[band][arfcn]
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_SetRxCalibTblReq
{
    GsmL1_FreqBand_t freqBand;      ///< GSM Frequency band
    uint8_t bUplink;                ///< Direction (0:Downlink, 1:Uplink)

    float fExtRxGain;               ///< External RX gain
    float fRxMixGainCorr;           ///< Mixer gain error compensation
    float fRxLnaGainCorr[3];        ///< LNA gain error compensation (1:@-12 dB, 2:@-24 dB, 3:@-36 dB)
    float fRxRollOffCorr[374];      /***< Frequency roll-off compensation
                                                for GSM-850 : 0=128, 1:129, ..., 123:251, [124-373]:unused
                                                for GSM-900 : 0=955, 1:956, ...,  70:1, ..., 317:956, [318-373]:unused
                                                for DCS-1800: 0=512, 1:513, ..., 373:885
                                                for PCS-1900: 0=512, 1:513, ..., 298:810, [299-373]:unused */                                                
    uint8_t u8IqImbalMode;          ///< IQ imbalance mode (0:off, 1:on, 2:auto)
    uint16_t u16IqImbalCorr[4];    	///< IQ imbalance compensation

    uint8_t u8DspMajVer;           	///< DSP firmware major version (0 if unkown)
    uint8_t u8DspMinVer;           	///< DSP firmware minor version (0 if unkown)
    uint8_t u8FpgaMajVer;          	///< FPGA firmware major version (0 if unkown)
    uint8_t u8FpgaMinVer;          	///< FPGA firmware minor version (0 if unkown)
} SuperFemto_SetRxCalibTblReq_t;

/****************************************************************************
 * Struct : SuperFemto_SetRxCalibTblCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the use of the new RX level calibration table.
 *
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_SetRxCalibTblCnf
{
    GsmL1_Status_t status;         
    GsmL1_FreqBand_t freqBand;      ///< GSM Frequency band
    uint8_t bUplink;                ///< Direction (0:Downlink, 1:Uplink)                                 
} SuperFemto_SetRxCalibTblCnf_t;       
       
/****************************************************************************
 * Struct : SuperFemto_MuteRfReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the RF clock sub-system.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_MuteRfReq
{       
   uint8_t u8Mute[8];			///< Timeslot mute flag (0:unmute, 1:mute) 

} SuperFemto_MuteRfReq_t;

/****************************************************************************
 * Struct : SuperFemto_MuteRfCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the RF clock sub-system.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_MuteRfCnf
{        
     SuperFemto_Status_t status;   ///< Status of the MUTE-RF-REQ

} SuperFemto_MuteRfCnf_t;

/****************************************************************************
 * Struct : SuperFemto_SetRxAttenReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the RF receive attenuation.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_SetRxAttenReq
{
   uint8_t u8Atten;				///< RX Attenuation: 0(default), 12, 24, 36 dB

} SuperFemto_SetRxAttenReq_t;

/****************************************************************************
 * Struct : SuperFemto_SetRxAttenCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the RF receive
 * attenuation.
 *
 * @ingroup superfemto_api_prim_dbg
 *
 ***************************************************************************/
typedef struct SuperFemto_SetRxAttenCnf
{
     SuperFemto_Status_t status;   ///< Status of the SET-RX-ATTEN-REQ

} SuperFemto_SetRxAttenCnf_t;

/****************************************************************************
 * Struct : SuperFemto_Primt_t
 ************************************************************************//**
 * 
 * System level primitive definition.
 * 
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/
typedef struct SuperFemto_Prim
{
    SuperFemto_PrimId_t id;                             ///< Primitive ID

    union 
    {     
        SuperFemto_SystemInfoCnf_t    systemInfoCnf;    ///< CNF: System information
        SuperFemto_SysemFailureInd_t  sysemFailureInd;  ///< IND: System failure indication   
        SuperFemto_ActivateRfReq_t    activateRfReq;    ///< REQ: Activation of the RF section
        SuperFemto_ActivateRfCnf_t    activateRfCnf;    ///< CNF: Activation of the RF section
        SuperFemto_DeactivateRfCnf_t  deactivateRfCnf;  ///< CNF: Deactivation of the RF section
        SuperFemto_SetTraceFlagsReq_t setTraceFlagsReq; ///< REQ: Set the verbosity of the system          
        SuperFemto_RfClockInfoReq_t   rfClockInfoReq;   ///< REQ: Information about the RF clock sub-system
        SuperFemto_RfClockInfoCnf_t   rfClockInfoCnf;   ///< CNF: Returns the RF clock sub-system information
        SuperFemto_RfClockSetupReq_t  rfClockSetupReq;  ///< REQ: Configure the RF clock sub-system
        SuperFemto_RfClockSetupCnf_t  rfClockSetupCnf;  ///< CNF: Confirm the configuration of the RF clock sub-system
        SuperFemto_Layer1ResetReq_t   layer1ResetReq;   ///< REQ: Trigger a reset of the GSM layer 1 module
        SuperFemto_Layer1ResetCnf_t   layer1ResetCnf;   ///< CNF: Confirm that the GSM layer 1 module was reset
        SuperFemto_GetTxCalibTblReq_t getTxCalibTblReq; ///< REQ: Get the TX gain calibration table
        SuperFemto_GetTxCalibTblCnf_t getTxCalibTblCnf; ///< CNF: Returns TX gain calibration table
        SuperFemto_SetTxCalibTblReq_t setTxCalibTblReq; ///< REQ: Set the TX gain calibration table
        SuperFemto_SetTxCalibTblCnf_t setTxCalibTblCnf; ///< CNF: Confirm the use of the new TX gain calibration table
        SuperFemto_GetRxCalibTblReq_t getRxCalibTblReq; ///< REQ: Get the RX level calibration table
        SuperFemto_GetRxCalibTblCnf_t getRxCalibTblCnf; ///< CNF: Returns RX level calibration table
        SuperFemto_SetRxCalibTblReq_t setRxCalibTblReq; ///< REQ: Set the RX level calibration table
        SuperFemto_SetRxCalibTblCnf_t setRxCalibTblCnf; ///< CNF: Confirm the use of the new RX level calibration table
        SuperFemto_MuteRfReq_t        muteRfReq;        ///< REQ: Mute/Unmute the RF section
        SuperFemto_MuteRfCnf_t        muteRfCnf;        ///< CNF: Confirm the mutin/unmiting of the the RF section
        SuperFemto_SetRxAttenReq_t    setRxAttenReq;    ///< REQ: Set the RX attenuation
        SuperFemto_SetRxAttenCnf_t    setRxAttenCnf;    ///< CNF: Confirm the configuration of the RX attenuation

    } u;

} SuperFemto_Prim_t;

 
#ifdef _TMS320C6400
 
/****************************************************************************
 *                               Public Functions                           * 
 ****************************************************************************/

/****************************************************************************
 * Function : SuperFemto_RxPrimCallback
 ************************************************************************//**
 * 
 * SuperFemto primitive reception callback routine. Process primitives sent
 * by the ARM processor.
 *
 * @param [in] pPrim
 *      Address of the buffer containing the received primitive.
 *
 * @return
 *      GsmL1_Status_Success or the error code
 *  
 * @ingroup superfemto_api_prim_sys
 *
 ****************************************************************************/ 
void SuperFemto_RxPrimCallback( SuperFemto_Prim_t *pPrim );
 
 
#endif  // _TMS320C6400

#ifdef __cplusplus
}
#endif  // extern "C"

#endif  // SUPERFEMTO_H__
