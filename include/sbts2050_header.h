#ifndef PACKET_H__
#define PACKET_H__

#ifdef __cplusplus
	extern "C" {
#endif

#include <stdint.h>

#define	RQT_SUCCESS	0

enum sbts2050_ids_request {
	SBTS2050_PWR_RQT = 0x00,
	SBTS2050_PWR_STATUS = 0x01,
	SBTS2050_TEMP_RQT = 0x10
};

typedef struct
{
    uint16_t u16Magic;              ///< Magic ID (0xCAFE)
    uint8_t  u8Id;                  ///< Command ID
    uint8_t  u8Len;                 ///< Command length in bytes (not including the parity)
    union {
        //
        // Power Supply commands
        //

        // Activate/deactivate power supply
        struct
        {
            uint8_t u1MasterEn  :1;     ///< Master SF enable (0:disable, 1:enable)
            uint8_t u1SlaveEn   :1;     ///< Slave SF enable (0:disable, 1:enable)
            uint8_t u1PwrAmpEn  :1;     ///< PA enable (0:disable, 1:enable)
        } __attribute__((packed)) pwrSetState;

        // Get the status of the power supplies
        struct
        {
        } __attribute__((packed)) pwrGetStatus;

        //
        // Temperature commands
        //

        // Get temperature
        struct
        {
        } __attribute__((packed)) tempGet;

        //
        // Ethernet Swicth commands
        //

        // Reset the ethernet switch
        struct
        {
        } __attribute__((packed)) ethswtReset;

        // Write to the ethernet switch EEPROM
        struct
        {
            uint8_t u8Addr;             ///< Write address
            uint8_t u8Data;             ///< Write value
        } __attribute__((packed)) ethswtEepromWrite;

        // Read from the ethernet switch EEPROM
        struct
        {
            uint8_t u8Addr;             ///< Write address
        } __attribute__((packed)) ethswtEepromRead;

        // Write to the MDIO port of the ethernet switch
        struct
        {
            uint8_t  u8Dev;             ///< Device address
            uint8_t  u8Reg;             ///< Register address
            uint16_t u16Data;           ///< Write data
        } __attribute__((packed)) ethswtMdioWrite;

        // Read from the MDIO port of the ethernet switch
        struct
        {
            uint8_t u8Dev;              ///< Device address
            uint8_t u8Reg;              ///< Write address
        } __attribute__((packed)) ethswtMdioRead;

        //
        // Watchdog commands
        //

        // Set the timeout value of the watchdog timer
        struct
        {
            uint8_t u8Timeout;          ///< Timout value in seconds (0: disable)
        } __attribute__((packed)) wdtSetTimeout;

        // Trig the  watchdog timer
        struct
        {
        } __attribute__((packed)) wdtTrig;

        //
        // Firmware commands
        //

        // Get firmware version
        struct
        {
        } __attribute__((packed)) fwGetVer;

        // Update firmware (enter programming mode)
        struct
        {
        } __attribute__((packed)) fwUpdate;

        // Restart the application
        struct
        {
        } __attribute__((packed)) fwReset;

        // Erase a flash section
        struct
        {
            uint32_t u32Addr;           ///< Flash address
	} __attribute__((packed)) fwFlErase;

        // Program flash memory
        struct
        {
            uint32_t u32Addr;           ///< Flash address
            uint8_t  u8Len;             ///< Number of bytes to be programmed
	    uint8_t  u8Data[128];       ///< Data to be programmed
        } __attribute__((packed)) fwFlProg;

        //
        // Raw payload
        //
        uint8_t raw[0];                 ///< Raw command data
    } cmd;
    uint8_t  u8Parity;              ///< Command parity

} __attribute__((packed)) cmdpkt_t;


/****************************************************************************
 * Struct : rsppkt_t
 ************************************************************************//**
 *
 * Response packet format (from control board).
 *
 ****************************************************************************/
typedef struct
{
    uint16_t u16Magic;              ///< Magic ID (0xCAFE)
    uint8_t  u8Id;                  ///< Command ID
    uint8_t  u8Len;                 ///< Response length in bytes (not including the parity)
    int8_t   i8Error;               ///< Error code (0:success, /0:error)
    union {
        //
        // Power Supply commands
        //

        // Activate/deactivate power supply
        struct
        {
        } __attribute__((packed)) pwrSetState;

        // Get the status of the power supplies
        struct
        {
            uint8_t u1MasterEn  :1;     ///< Master SF enable (0:disable, 1:enable)
            uint8_t u1SlaveEn   :1;     ///< Slave SF enable (0:disable, 1:enable)
            uint8_t u1PwrAmpEn  :1;     ///< PA enable (0:disable, 1:enable)

            uint8_t u8MasterV;          ///< Master SF voltage (Q3.5)
            uint8_t u8MasterA;          ///< Master SF current (Q2.6)
            uint8_t u8SlaveV;           ///< Slave SF voltage (Q3.5)
            uint8_t u8SlaveA;           ///< Slave SF current (Q2.6)
            uint8_t u8PwrAmpV;          ///< PA voltage (Q6.2)
            uint8_t u8PwrAmpA;          ///< PA current (Q2.6)
            uint8_t u8PwrAmpBiasV;      ///< PA Bias voltage (Q4.4)
            uint8_t u8MainSupplyA;      ///< Main supply (24V) current (Q2.6)
       } __attribute__((packed)) pwrGetStatus;

        //
        // Temperature commands
        //

        // Get temperature
        struct
        {
            int8_t  i8BrdTemp;          ///< Control board temperature
            int8_t  i8PaTemp;           ///< PA temperature
        } __attribute__((packed)) tempGet;

        //
        // Ethernet Swicth commands
        //

        // Reset the ethernet switch
        struct
        {
        } __attribute__((packed)) ethswtReset;

        // Write to the ethernet switch EEPROM
        struct
        {
        } __attribute__((packed)) ethswtEepromWrite;

        // Read from the ethernet switch EEPROM
        struct
        {
            uint8_t u8Value;            ///< Read value
        } __attribute__((packed)) ethswtEepromRead;

        // Write to the MDIO port of the ethernet switch
        struct
        {
        } __attribute__((packed)) ethswtMdioWrite;

        // Read from the MDIO port of the ethernet switch
        struct
        {
            uint16_t u16Data;           ///< Read value
        } __attribute__((packed)) ethswtMdioRead;

        //
        // Watchdog commands
        //

        // Set the timeout value of the watchdog timer
        struct
        {
        } __attribute__((packed)) wdtSetTimout;

        // Trig the  watchdog timer
        struct
        {
        } __attribute__((packed)) wdtTrig;

        //
        // Firmware commands
        //

        // Get firmware version
        struct
        {
            uint8_t u8Major;            ///< Major version number
            uint8_t u8Minor;            ///< Minot version number
        } __attribute__((packed)) fwGetVer;

        // Update firmware
        struct
        {
        } __attribute__((packed)) fwUpdate;

        // Restart the application
        struct
        {
        } __attribute__((packed)) fwReset;

        // Erase a flash section
        struct
        {
        } __attribute__((packed)) fwFlErase;

        // Program flash memory
        struct
        {
        } __attribute__((packed)) fwFlProg;
 
        //
        // Raw payload
        //
        uint8_t raw[0];                 ///< Raw command data
    } rsp;
    uint8_t  u8Parity;              ///< Command parity
} __attribute__((packed)) rsppkt_t;

#ifdef __cplusplus
}
#endif
#endif
