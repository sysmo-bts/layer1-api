#ifndef GSML1TYPES_H__
#define GSML1TYPES_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                  Includes                                *
 ****************************************************************************/

#ifndef WIN32
    #include <stdint.h>
#endif

#include "gsml1const.h"

/****************************************************************************
 *                                  Types                                   *
 ****************************************************************************/

/****************************************************************************
 * typedef : HANDLE
 ************************************************************************//**
 *
 * Generic type used to identify an entity.
 *   
 * @ingroup gsml1_api_types
 *
 ***************************************************************************/
typedef uint32_t HANDLE;

/****************************************************************************
 * typedef : GsmL1_LogChParam_t
 ************************************************************************//**
 *
 * Logical channel configuration parameters
 *
 * @ingroup gsml1_api_prim_params
 *
 ***************************************************************************/
typedef union
{
    // RACH
    struct
    {
        uint8_t u8Bsic;             ///< Base Station Identity Code
    } rach;

    // AGCH
    struct
    {
        uint8_t u8NbrOfAgch;        /**< Number of AGCH blocks on a CCCH
                                        @see BS_AG_BLKS_RES parameter in 3GPP TS 45_02 Section 7 Table 5 */
    } agch;

    // SACCH
    struct
    {
        uint8_t u8MsPowerLevel;     /**< Power level control
                                         @see TS 3GPP 45.005 - 4.1.1 Mobile station output power */
    } sacch;

    // TCH
    struct
    {
        GsmL1_TchPlFmt_t     tchPlFmt;               ///< Payload format (RFC RTP, Etsi IF2)
        GsmL1_TchPlType_t    tchPlType;              ///< Expected payload type (HR, FR, EFR or AMR)
        GsmL1_AmrCmiPhase_t  amrCmiPhase;            ///< AMR Codec Mode Indication phase (downlink)
        GsmL1_AmrCodecMode_t amrInitCodecMode;       ///< AMR Initial Codec Mode
        GsmL1_AmrCodec_t     amrActiveCodecSet[4];   ///< AMR Active Codec Set
    } tch;

    // PRACH
    struct
    {
        uint8_t u8Bsic;             ///< Base Station Identity Code
    } prach;

    // PTCCH
    struct
    {
        uint8_t u8Bsic;             ///< Base Station Identity Code
    } ptcch;

} GsmL1_LogChParam_t;

/****************************************************************************
 * typedef : GsmL1_ConfigParam_t
 ************************************************************************//**
 *
 * Layer 1 configuration parameters
 *
 * @ingroup gsml1_api_prim_params
 *
 ***************************************************************************/
typedef union
{
    // Set the normal burst TSC of the specified RF device
    struct
    {
        uint8_t u8NbTsc;                ///< Normal burst training sequence code of the specified RF device

    } setNbTsc;

    // Set the TX power level of the specified RF device
    struct
    {
        float fTxPowerLevel;            ///< Transmitter power of the specified RF device

    } setTxPowerLevel;

    // Set the logical channel specific parameters
    struct
    {
        GsmL1_Sapi_t       sapi;        ///< Service access point identifier
        uint8_t            u8Tn;        ///< Time-slot [0..7]
        GsmL1_SubCh_t      subCh;       ///< Sub-channel number (0x1F if not applicable)
        GsmL1_Dir_t        dir;         ///< Direction path to configure
        GsmL1_LogChParam_t logChParams; ///< Logical channel parameters

    } setLogChParams;

    ///< Configure the ciphering parameters
    struct 
    {
        uint8_t             u8Tn;       ///< Time-slot [0..7]
        GsmL1_SubCh_t       subCh;      ///< Sub-channel number (0x1F if not applicable)
        GsmL1_Dir_t         dir;        ///< Direction path to configure
        GsmL1_CipherId_t    cipherId;   ///< Ciphering algorithm
        uint8_t             u8Kc[8];    ///< 64-bit ciphering key 
    } setCipheringParams;
    
} GsmL1_ConfigParam_t;
          
#ifdef __cplusplus
}
#endif  // extern "C"

#endif    // GSML1TYPES_H__
